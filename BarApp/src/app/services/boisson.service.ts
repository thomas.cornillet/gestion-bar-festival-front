import {EventEmitter, Injectable} from '@angular/core';
import {Boisson} from "../models/boisson";
import {Soft} from "../models/soft";
import {Beer} from "../models/beer";

@Injectable({
  providedIn: 'root'
})
export class BoissonService {
  private boissons: Boisson[] = [];
  public emetteurBoissons: EventEmitter<Boisson[]> = new EventEmitter<Boisson[]>();

  constructor() {
    this.initBoissonsTest();
  }

  notify() {
    this.emetteurBoissons.emit(this.boissons.slice());
  }

  getBoissons(): Boisson[] {
    return this.boissons;
  }

  // TODO ne pas conserver une fois relier à l'API
  initBoissonsTest() {
    let orangina: Boisson = new Soft(true, "soda");
    orangina.idBoisson = 1;
    orangina.nomBoisson = "Orangina";
    orangina.tarif = ["2","4","10"];
    orangina.stock = 300;
    orangina.pichet = true;
    orangina.categorie = "Soft";
    this.boissons.push(orangina);

    let bzhCola: Boisson = new Soft(true, "soda");
    bzhCola.idBoisson = 2;
    bzhCola.nomBoisson = "Breizh Cola";
    bzhCola.tarif = ["2","4","10"];
    bzhCola.stock = 300;
    bzhCola.pichet = true;
    bzhCola.categorie = "Soft";
    this.boissons.push(bzhCola);

    let bzhTea: Boisson = new Soft(false, "icetea");
    bzhTea.idBoisson = 3;
    bzhTea.nomBoisson = "Breizh Tea";
    bzhTea.tarif = ["2","4","10"];
    bzhTea.stock = 300;
    bzhTea.pichet = true;
    bzhTea.categorie = "Soft";
    this.boissons.push(bzhTea);

    let kro: Boisson = new Beer(4.2, "pils");
    kro.idBoisson = 4;
    kro.nomBoisson = "Kronenbourg";
    kro.tarif = ["2","4","10"];
    kro.stock = 300;
    kro.pichet = true;
    kro.categorie = "Beer";
    this.boissons.push(kro);

    let eroica: Boisson = new Beer(6.7, "ipa");
    eroica.idBoisson = 5;
    eroica.nomBoisson = "Eroica";
    eroica.tarif = ["3","6"];
    eroica.stock = 150;
    eroica.pichet = false;
    eroica.categorie = "Beer";
    this.boissons.push(eroica);

    let weihenStefan: Boisson = new Beer(5.4, "weizen");
    weihenStefan.idBoisson = 6;
    weihenStefan.nomBoisson = "Weihenstephaner Hefe Weissbier";
    weihenStefan.tarif = ["2.5","5","13"];
    weihenStefan.stock = 200;
    weihenStefan.pichet = true;
    weihenStefan.categorie = "Beer";
    this.boissons.push(weihenStefan);

  }
}
