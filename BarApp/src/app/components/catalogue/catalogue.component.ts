import {Component, OnDestroy, OnInit} from '@angular/core';
import {Boisson} from "../../models/boisson";
import {BoissonService} from "../../services/boisson.service";

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit, OnDestroy {
  catalogue!: Boisson[];

  constructor(private boissonsService: BoissonService) { }

  ngOnInit(): void {
    this.boissonsService.emetteurBoissons.subscribe( (value) => this.catalogue = value);
    this.catalogue = this.boissonsService.getBoissons();
  }

  ngOnDestroy(): void {
    this.boissonsService.emetteurBoissons.unsubscribe();
  }

}
