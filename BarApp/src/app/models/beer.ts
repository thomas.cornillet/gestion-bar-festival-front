import {Boisson} from "./boisson";

export class Beer extends Boisson{
  // alcool!: number;
  // style!: string;

  constructor(public alcool: number, public style: string) {
    super();
  }

  override toString(): string {
    return "[Beer] " + super.toString() + " [alcool: " + this.alcool + ", style: " + this.style + "]";
  }

}
