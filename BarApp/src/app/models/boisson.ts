export class Boisson {
  idBoisson!: number;
  nomBoisson!: string;
  tarif!: string[]; // TODO peut-être multiplier les tarif pour ne pas avoir de tableau de string mais tarifDemi, tarifPinte, tarifPichet
  stock!: number;
  pichet!: boolean;
  categorie!: string;

  constructor() {} // TODO revoir s'il ne vaudrait mieux pas un contructeur avec les variables (idem pour Beer et Soft)

  toString(): string {
    return "Boisson [idBoisson:" + this.idBoisson + ", nom:" + this.nomBoisson + ", idTarif: " + this.tarif + ", stock: " + this.stock
      + ", pichet: " + this.pichet + ", categorie: " + this.categorie + "]";
  }
}
