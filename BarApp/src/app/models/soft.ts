import {Boisson} from "./boisson";

export class Soft extends Boisson {
  // gaz!: boolean;
  // type!: string;

  constructor(public gaz: boolean, public type: string) {
    super();
    // this.gaz = gaz;
    // this.
  }

  override toString(): string {
    let gazeux: string = this.gaz ? "gazeux" : "plat"; // ça ne fonctionne avec la ternaire dans le return, ça n'affiche que le résultat de la ternaire
    return "[Soft] " + super.toString() + " [gaz: " + gazeux + ", type: " + this.type + "]";
  }

}
